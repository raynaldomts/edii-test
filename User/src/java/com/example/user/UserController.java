@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users/{userid}")
    public ResponseEntity<User> getUser(@PathVariable("userid") Integer userid) {
        User user = userRepository.findById(userid).orElse(null);
        return ResponseEntity.ok(user);
    }

    @PostMapping("/users")
    public ResponseEntity<User> saveUser(@RequestBody User user) {
        userRepository.save(user);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/users/{userid}")
    public ResponseEntity<Void> deleteUser(@PathVariable("userid") Integer userid) {
        userRepository.deleteById(userid);
        return ResponseEntity.noContent().build();
    }
}
